extends Label
var start_time

func _on_game_manager_game_over():
	var end_time = OS.get_system_time_secs()
	self.visible = true
	text = "Score: " + str(end_time - start_time)


func _on_game_manager_game_start():
	self.visible = false
	start_time = OS.get_system_time_secs()
