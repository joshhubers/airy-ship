extends KinematicBody2D

signal death

const GRAVITY = 200.0
const JUMP_FORCE = 50.0
var velocity = Vector2()
var playing = false
var view
var collision_shape

func out_of_play():
	var center_y = get_position().y
	var top_of_character = center_y - collision_shape.shape.extents.y
	var bottom_of_character = center_y + collision_shape.shape.extents.y
	
	if bottom_of_character < 0:
		return true
	if top_of_character > view.y:
		return true
	
	return false
	
func _ready():
	view = get_viewport().size
	collision_shape = find_node("CollisionShape2D")

func _physics_process(delta):
	if not playing:
		return

	velocity.y += delta * GRAVITY

	var motion = velocity * delta
	var collision = move_and_collide(motion)
	if collision or out_of_play() and playing:
		emit_signal("death")
	
func _input(ev):
	if ev is InputEventKey and ev.scancode == KEY_SPACE and not ev.echo and playing:
		velocity.y -= JUMP_FORCE

func _on_game_manager_game_start():
	set_position(Vector2(100, 200))
	playing = true
	velocity = Vector2()

func _on_game_manager_game_over():
	playing = false
