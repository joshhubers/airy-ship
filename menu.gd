extends Control

func _on_game_manager_game_start():
	self.visible = false

func _on_game_manager_game_over():
	self.visible = true
