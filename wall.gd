extends KinematicBody2D

signal wall_hit

var SPEED = -200.0
var enabled = true

func _physics_process(delta):
	if enabled:
		var motion = Vector2(SPEED, 0) * delta
		var col = move_and_collide(motion)
		if col:
			emit_signal("wall_hit")

func _on_game_manager_game_over():
	enabled = false
	queue_free()
