extends Node

signal game_start
signal game_over

var playing = false
var GAP = 25
var SPAWN_X = 1100
var SPAWN_TIME = 2
var timer = Timer.new()
var rng = RandomNumberGenerator.new()
var root
var wall
var view
var half_gap

# Called when the node enters the scene tree for the first time.
func _ready():
	root = get_tree().get_root()
	wall = preload("res://wall.tscn")
	view = get_viewport().size
	timer.connect("timeout", self, "_on_timer_timeout")
	add_child(timer)
	half_gap = GAP/2
	
func start_game():
	timer.start(SPAWN_TIME)
	emit_signal("game_start")

func _on_timer_timeout():
	var top_wall = wall.instance()
	top_wall.connect("wall_hit", self, "game_over")
	connect("game_over", top_wall, "_on_game_manager_game_over")
	top_wall.visible = true
	top_wall.add_to_group("walls")
	top_wall.rotation_degrees = 180
	
	var wall_height = top_wall.get_node("CollisionShape2D").shape.extents.y
	
	var center_y = rng.randf_range(GAP, view.y-GAP-wall_height)
	
	var bottom_margin_of_top_wall = center_y - half_gap - wall_height
	var scale_factor_top = bottom_margin_of_top_wall / (wall_height*2)
	top_wall.set_scale(Vector2(1, scale_factor_top))
	top_wall.set_position(Vector2(SPAWN_X, bottom_margin_of_top_wall/2))
	root.add_child(top_wall)

	var bottom_wall = wall.instance()
	connect("game_over", bottom_wall, "_on_game_manager_game_over")
	bottom_wall.connect("wall_hit", self, "game_over")
	bottom_wall.visible = true
	bottom_wall.add_to_group("walls")
	var top_margin_of_bottom_wall = center_y + half_gap + wall_height
	var top_diff = view.y - top_margin_of_bottom_wall
	var scale_factor_bottom = top_diff / (wall_height*2)
	bottom_wall.set_scale(Vector2(1, scale_factor_bottom))
	var bottom_y = top_margin_of_bottom_wall + (top_diff/2)
	bottom_wall.set_position(Vector2(SPAWN_X, bottom_y))
	root.add_child(bottom_wall)

func _on_Button_pressed():
	start_game()

func _on_character_death():
	game_over()

func game_over():
	emit_signal("game_over")
	timer.stop()
